"""
rssfeed_parse.py

Øvelses instruktioner: https://ucl-pba-its.gitlab.io/24e-its-intro/exercises/12_intro_opgave_python_nw/

Husk at kigge på filen hackernews_feed.md som genereres af programmet
"""

import feedparser
import json
from bs4 import BeautifulSoup
import re

# RSS URL's
feed_url = 'http://feeds.feedburner.com/TheHackersNews'

# instantiate objects
rss_feed = feedparser.parse(feed_url)

#print entire feed
# print(rss_feed.feed)

#print list of feed entries with json module to make it look nice
# print(json.dumps(rss_feed.entries, indent = 1))

#print first item in list of feed entries
# print(json.dumps(rss_feed.entries[0], indent = 1))

# print(type(rss_feed.entries))
# print(len(rss_feed.entries))
file_entry_list = []

for entry in rss_feed.entries[0:len(rss_feed.entries)]:
    soup = BeautifulSoup(entry.summary_detail.value, 'html.parser')
    file_entry_list.append(
        f'# {entry.title}\n**_Author: {entry.author}_**  \nPublished:  {entry.published}  \n**_Summary_**  \n{re.sub(r"[^a-zA-Z0-9]", " ", entry.summary).replace(" adsense ", " ").replace(" lt ", " ").replace(" gt ", " ")}  \n  \nLink to full article:  \n[{entry.link}]({entry.link})\n'
        )
    # print(f'\nTitle: \n{entry.title}, \nAuthor: \n{entry.author}, \nPublished: \n{entry.published}\nSummary: \n{entry.summary}')

print(file_entry_list)
with open('hackernews_feed.md', 'w') as file:
     file.write(''.join(file_entry_list))