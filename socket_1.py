"""
Socket_1.py

Øvelses instruktioner: https://ucl-pba-its.gitlab.io/24e-its-intro/exercises/12_intro_opgave_python_nw/
"""

import socket

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# input url, use default if nothing entered
url = input('Enter a url: ')
if url == "":
    url = 'http://data.pr4e.org/romeo.txt'

# extract domain from url
domain = url.split("//")[-1].split("/")[0].split('?')[0]

# connect a socket on port 80 to the domain
mysock.connect((domain, 80))

# build and encode the http GET command
cmd = 'GET ' + url + ' HTTP/1.0\r\n\r\n'
cmd_encoded = cmd.encode()

# send the byte encoded GET command
mysock.send(cmd_encoded)

# receive 512 chars at a time, decode and print until empty line received
while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    print(data.decode(),end='')

# close socket
mysock.close()