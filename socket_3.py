"""
Socket_3.py

Øvelses instruktioner: https://ucl-pba-its.gitlab.io/24e-its-intro/exercises/12_intro_opgave_python_nw/

Written by: Lasse Justesen - 2021
"""

import socket

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mysock.connect(('data.pr4e.org', 80))
cmd = 'GET http://data.pr4e.org/intro-short.txt HTTP/1.0\r\n\r\n'.encode()
mysock.send(cmd)

received =b"" #Stores recieved data in bytes

while True:
    data = mysock.recv(512)
    if len(data) < 1:
        break
    received += data
pos = received.find(b"\r\n\r\n") #Look for position of header & blank line
content = received[pos+4:].decode() 
print(content)
mysock.close()
