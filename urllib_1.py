"""
urllib_1.py

Øvelses instruktioner: https://ucl-pba-its.gitlab.io/24e-its-intro/exercises/12_intro_opgave_python_nw/
"""

import urllib.request

url = input('Enter a url: ')
if url == "":
    url = 'https://www.w3.org/Protocols/rfc2616/rfc2616.txt'

fhand = urllib.request.urlopen(url)
char_count = 0
chars_printed = 0

for line in fhand:
    line_split = line.decode().split()

    if not line_split:
        continue
    else:
        for item in line_split:
            char_count += (len(item))

    if char_count <= 3000:
        print(line.decode().strip())
        chars_printed = char_count

print(f'chars received: {char_count}, chars printed {chars_printed}')